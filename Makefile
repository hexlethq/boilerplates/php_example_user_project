start:
	php -S 0.0.0.0:8000 -t public

test:
	composer exec --verbose phpunit tests

install:
	composer install
